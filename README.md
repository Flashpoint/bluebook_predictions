This is the python source code for my solution of the *Bluebook for Bulldozers*, Kaggle competition.


Dependencies: (not the lowest version needed, just the ones I used on my machine) 


- Python 2.7.3
- Numpy 1.7.0
- Pandas 0.10.0
- Orange 2.5
- Sklearn 0.13


Regards,
Vid
